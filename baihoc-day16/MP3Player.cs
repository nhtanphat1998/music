﻿using NAudio.Wave;

class MP3Player
{
    string fileName = "music.txt";
    public void PlayMusic(string filePath)
    {
        var audioFile = new AudioFileReader(filePath);
        var outputDevice = new WaveOutEvent();
        outputDevice.Init(audioFile);
        outputDevice.Play();
        Console.WriteLine("Music is playing: " + filePath);

        while (outputDevice.PlaybackState == PlaybackState.Playing)
        {
            string input;
            do
            {
                Console.WriteLine("---------------");
                Console.WriteLine("Press keyword:");
                Console.WriteLine("1. next: move to next song");
                Console.WriteLine("2. back: back to previous song");
                Console.WriteLine("3. pause: pause current song");
                Console.WriteLine("4. play: play current song");
                Console.WriteLine("5. stop: stop audio");
                Console.Write("Your choice: ");
                input = Console.ReadLine();
                if (input == "pause")
                {
                    outputDevice.Pause();
                }
                else if (input == "play")
                {
                    outputDevice.Play();
                }
                else if (input == "stop")
                {
                    outputDevice.Stop();
                }
                else if (input == "next")
                {
                    string[] lines = File.ReadAllLines(fileName);
                    for (int i = 0; i < lines.Length; i++)
                    {
                        if (lines[i] == filePath)
                        {
                            if ((i + 1) == lines.Length)
                            {
                                Console.WriteLine("There are no more songs");
                                break;
                            }
                            outputDevice.Stop();
                            audioFile = new AudioFileReader(lines[i + 1]);
                            outputDevice = new WaveOutEvent();
                            outputDevice.Init(audioFile);
                            outputDevice.Play();
                            break;
                        }
                    }
                }
                else if (input == "back")
                {
                    string[] lines = File.ReadAllLines(fileName);
                    for (int i = 0; i < lines.Length; i++)
                    {

                        if (lines[i] == filePath)
                        {
                            if ((i - 1) < 0)
                            {
                                Console.WriteLine("There are no previous songs");
                                break;
                            }
                            outputDevice.Stop();
                            audioFile = new AudioFileReader(lines[i - 1]);
                            outputDevice = new WaveOutEvent();
                            outputDevice.Init(audioFile);
                            outputDevice.Play();
                            break;
                        }
                    }
                }
            } while (input != "stop");
        }
        Console.WriteLine("Audio playback finished: " + filePath);
    }

    public void ViewListSongs()
    {
        try
        {
            var mp3Player = new MP3Player();
            string[] lines = File.ReadAllLines(fileName);
            for (int i = 0; i < lines.Length; i++)
            {
                Console.WriteLine((i + 1) + ". " + lines[i]);
            }

        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
        }
    }
    public void ChooseSong()
    {
        ViewListSongs();
        bool check = false;
        do
        {
            Console.Write("Choose ur song: ");
            try
            {
                var number = int.Parse(Console.ReadLine());

                var mp3Player = new MP3Player();
                string[] lines = File.ReadAllLines(fileName);
                for (int i = 0; i < lines.Length; i++)
                {
                    if ((i + 1) == number)
                    {
                        mp3Player.PlayMusic(lines[i]);
                        check = true;
                        break;
                    }
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                check = false;
            }
        } while (!check);
    }
}